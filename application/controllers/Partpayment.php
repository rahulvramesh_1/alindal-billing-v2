<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partpayment extends CI_Controller {
	public function __construct()
    {
           // Call the CI_Model constructor
        parent::__construct();
	   $this->load->model('Payment_model');
	   $this->load->library('parser');
	}
	public function index(){
		$this->load->model('Customer_model');
		$data['customers'] = $this->Customer_model->get_customer();
		$this->load->view('common/header.php');
		$this->load->view('payment/index.php',$data);
		
		$this->load->view('common/footer.php');
		}
		public function get_credit(){
			$this->load->model('Payment_model');
			$customer_id=$this->input->post('customer_name');
			$result=$this->Payment_model->get_credit($customer_id);
			foreach ($result as  $value) {
				$cred=$value['credit'];
			}
			// print_r($result);
			// echo $result;
			echo $cred;
			// echo $customer;
		}
		public function do_payment(){
			// echo "string";
			$data=array('customer_id'=>$this->input->post('customer_name'),
						 'credit'=>$this->input->post('credit'),
						 'cash_received'=>$this->input->post('cash_received'));
			// print_r($data);
			$balance=$data['credit']-$data['cash_received'];
			$result=$this->Payment_model->do_payment($data);
			$reduction=$this->Payment_model->update_credit($data['credit'],$balance);
		}

}