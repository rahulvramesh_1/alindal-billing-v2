<?php
class Report_model extends CI_Model {

	public function getDailyReport($date)
	{
		$date = '"'.$date.'"';
		$sql = "SELECT customers.name, customers.phone, customers.credit, orders.total, orders.discount, orders.payment, orders.created_at
				FROM  `orders` 
				LEFT JOIN customers ON customers.id = orders.customer_id
				WHERE DATE( orders.created_at ) = DATE($date) ";

	    $query = $this->db->query($sql);
	   
	    return $query->result_array();


	}

	public function cashOnHand($date,$amount)
	{
		$date = '"'.$date.'"';

		$sql = "SELECT * FROM `cash_on_hand` WHERE DATE( date ) = DATE($date) ";

	    $query = $this->db->query($sql);
	   
	    if($query->result_array())
	    {
	    	$sql = "UPDATE `cash_on_hand` SET `amount` = ".$amount." WHERE DATE( date ) = DATE($date)";
	    	$this->db->query($sql);
	    }
	    else
	    {
	    	$sql = "INSERT INTO `cash_on_hand` ( `date`, `amount`) VALUES (".$date.",".$amount.")";
	    	$this->db->query($sql);
	    }
	    

	}

	public function getCashOnHand($date)
	{
		$date = '"'.$date.'"';

		$sql = "SELECT `amount` FROM `cash_on_hand` WHERE DATE( date ) = DATE($date) ";

	    $query = $this->db->query($sql);
	   
	    $amount =  $query->result_array();

	    if($amount){
	    	$amount = $amount['0']['amount'];
	    }
	    else{
	    	$amount = 0;
	    }
	    

	    return $amount;
	}
	
}